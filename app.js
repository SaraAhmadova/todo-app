import express from "express";
import {v4 as generatedId} from 'uuid';
import dotenv from 'dotenv';
dotenv.config();

const app = express();

app.use(express.json());
const status = {
    0: 'todo',
    1: 'doing',
    2: 'done'
}
 
let TODOS = [
    {
        "id": "cf0086cd-d5ca-4f3d-b9ba-9a54f6af1418",
        "title": "Create project",
        "desc": "creat all steps",
        "status": "todo"
    },
    {
        "id": "575d498f-3c1b-4fa8-b83e-706d7b3fa076",
        "title": "Run project",
        "desc": "Check all steps",
        "status": "doing"
    },
    {
        "title": "Create version 6",
        "desc": "upgrade all",
        "status": "done",
        "id": "1a26cbfe-633b-42a7-a45e-f29a866e7495"
    }
]

app.get('/todos', (req,res)=>{
    return res.json(TODOS);
})

app.post('/todos/add', (req, res)=>{
   const id = generatedId();
   TODOS.push({...req.body, id, status: status[req.body.status] });
   return res.json('Added successfully...');
})

app.put('/todos/:id', (req, res)=>{
    TODOS = TODOS.map(todo=>todo.id === req.params.id ? {...req.body}: todo);
    return res.json("Updated successfully!");
})
app.patch('/todos/:id', (req, res)=>{
    TODOS = TODOS.map(todo=>todo.id === req.params.id ? {...req.body}: todo);
    return res.json("Updated successfully!");
})

app.delete('/todos/:id', (req, res)=>{
    TODOS = TODOS.filter(e=> e.id !== req.params.id);
    return res.json('Deleted successfully...');
})

app.listen(process.env.PORT, ()=>{
    console.log(`${process.env.PORT} port is running...`);
})

